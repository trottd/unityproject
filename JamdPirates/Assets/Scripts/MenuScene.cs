﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScene : MonoBehaviour {

    private CanvasGroup fadeGroup;
    private float fadeInSpeed = 0.33f;

    public RectTransform menuContainer;
    public Transform levelPanel;

    public Transform colorPanel;
    public Transform trailPanel;

    public Vector3 desiredMenuPosition;

    private void Start()
    {
        // grab teh only canvasgroup in the scene
        fadeGroup = FindObjectOfType<CanvasGroup>();

        // Start with a white screen
        fadeGroup.alpha = 1;

        // Add button on-click events to minigames buttons
        InitMini();

        // Add buttons on-click to level panel
        InitLevel();
    }

    private void Update()
    {
        // Fade in
        fadeGroup.alpha = 1 - Time.timeSinceLevelLoad * fadeInSpeed;
    }

    private void InitMini()
    {
        // Just make sure assigned the references
        if (colorPanel == null || trailPanel == null)
            Debug.Log("You didn't asign the inspector");

        // For every children transform under our color panel, find the button and add onclick
        int i = 0;
        foreach(Transform t in colorPanel)
        {
            int currentIndex = i;

            Button b = t.GetComponent<Button>();
            b.onClick.AddListener(() => OnColorSelect(currentIndex));

            i++;
        }

        // Reset Index
        i = 0;
        // Do the same for the trail panel
        foreach (Transform t in trailPanel)
        {
            int currentIndex = i;

            Button b = t.GetComponent<Button>();
            b.onClick.AddListener(() => OnTrailSelect(currentIndex));

            i++;
        }
    }

    private void InitLevel()
    {
        // Just make sure assigned the references
        if (levelPanel == null)
            Debug.Log("You didn't asign the inspector");

        // For every children transform under our level panel, find the button and add onclick
        int i = 0;
        foreach (Transform t in levelPanel)
        {
            int currentIndex = i;

            Button b = t.GetComponent<Button>();
            b.onClick.AddListener(() => OnLevelSelect(currentIndex));

            i++;
        }
    }


    // Buttons
    public void OnPlayClick()
    {
        Debug.Log("Play button has been clicked");
    }

    public void OnMiniGameClick()
    {
        Debug.Log("MiniGame button has been clicked");
    }
    private void OnTrailSelect(int currentIndex)
    {
        Debug.Log("Selecting color button : " + currentIndex);
    }
    private void OnColorSelect(int currentIndex)
    {
        Debug.Log("Selecting trail button : " + currentIndex);
    }

    public void OnColorUnLockSet()
    {
        Debug.Log("Buy/Set color");
    }
    
    public void OnTrailUnlockSet()
    {
        Debug.Log("Buy/Set trail");
    }
    private void OnLevelSelect(int currentIndex)
    {
        Debug.Log("Selecting Level : " + currentIndex);
    }
}
